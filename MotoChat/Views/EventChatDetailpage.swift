//
//  EventChatDetailpage.swift
//  MotoChat
//
//  Created by Justin Bunde on 27.07.23.
//

import SwiftUI

struct EventChatDetailpage: View {
    @ObservedObject var tourDetailpageViewModel = TourDetailpageViewModel()
    @Binding var selectedMessage: EventMessage
    @State var cafeImages = ["cafe1", "cafe2", "road1", "road2"]
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 16) {
                Text(selectedMessage.sender)
                    .font(.title)
                Image("pb")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 120, height: 120)
                    .cornerRadius(70)
                Text("Addresse:")
                    .font(.headline)
                Text("Tegeler Straße 23, Wedding, 13353 Berlin, Deutschland")
                Text("Website:")
                    .font(.headline)
                Text("https://www.cafeschade.de/")
                Text("Telefonnummer:")
                    .font(.headline)
                Button {
                    guard let number = URL(string: tourDetailpageViewModel.convertPhoneNumberFormat(phoneNumber: "+49 30 45797662")) else { return }
                    UIApplication.shared.open(number)
                } label: {
                    Text("+49 30 45797662")
                }

               
                Text("Bildergallerie:")
                    .font(.headline)
                ScrollView(.horizontal) {
                    HStack(spacing: 10) {
                        ForEach(cafeImages, id: \.self) { imageName in
                            Image(imageName)
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(width: 150, height: 150)
                                .cornerRadius(10)
                        }
                    }
                }
            }
            .padding(.all, 20)
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("Cafe Detail")
    }
}

//struct EventChatDetailpage_Previews: PreviewProvider {
//    static var previews: some View {
//        EventChatDetailpage()
//    }
//}
