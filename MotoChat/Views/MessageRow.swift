//
//  MessageRow.swift
//  MotoChat
//
//  Created by Justin Bunde on 26.07.23.
//

import SwiftUI

struct MessageRow: View {
    var message: EventMessage
    var isCurrentUser: Bool // Determine if the message belongs to the current user
    
    var body: some View {
        HStack {
            if isCurrentUser {
                Spacer()
            }
            
            VStack(alignment: isCurrentUser ? .trailing : .leading) {
                Text(message.sender)
                    .font(.caption)
                
                Text(message.text)
                    .padding(.vertical, 8)
                    .padding(.horizontal, 12)
                    .background(isCurrentUser ? Color.blue : Color.gray) // Use blue background for current user's message, and gray for others
                    .foregroundColor(.white) // White text color for better contrast
                    .cornerRadius(16)
            }
            .padding(.horizontal, 8)
            
            if !isCurrentUser {
                Spacer()
            }
        }
        .padding(.vertical, 4)
    }
}
