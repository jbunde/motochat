//
//  RegisterStationView.swift
//  MotoChat
//
//  Created by Justin Bunde on 26.07.23.
//

import SwiftUI
import MapKit
import CoreLocation

struct RegisterStationView: View {
    @ObservedObject var registerStationViewModel = RegisterStationViewModel()
    @ObservedObject var stationViewModel = StationViewModel()
    @Environment(\.presentationMode) var presentationMode
    @State var searchQuery = ""
    @State var showSelectedText = false
    @State var selectedBusiness = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)))
    var body: some View {
        NavigationView {
            ScrollView {
                TextField("Wie heißt dein Geschäft?", text: $searchQuery)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .onChange(of: searchQuery) { query in
                        registerStationViewModel.searchPlaces(query: query)
                    }.padding(.bottom, 15)
                
                LazyVStack(alignment: .leading) {
                    ForEach(registerStationViewModel.places, id: \.self) { place in
                        VStack(alignment: .leading) {
                            Text(place.name ?? "")
                                .font(.headline)
                            Text(place.placemark.title ?? "")
                                .font(.subheadline)
                            Button {
                                selectedBusiness = place
                                showSelectedText.toggle()
                            } label: {
                                Text("auswählen")
                            }.padding()
                        }.padding(.top, 2)
                    }.padding()
                }
                if showSelectedText {
                    Text("\(selectedBusiness.name ?? "") in \(selectedBusiness.placemark.title ?? "")").padding().fontWeight(.bold)
                    Button {
                        stationViewModel.addBusiness(annotation: Annotation(name: selectedBusiness.name ?? "default value" ?? "default value", coordinate: CLLocationCoordinate2D(latitude: selectedBusiness.placemark.coordinate.latitude, longitude: selectedBusiness.placemark.coordinate.longitude), phoneNumber: selectedBusiness.phoneNumber, website: selectedBusiness.url, isRegistered: true, address: selectedBusiness.placemark.title ?? "default value", isCafe: true))
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        Text("Cafe hinzufügen")
                    }
                }
            }.padding()
                .navigationTitle("Business registrieren")
        }
    }
}


//struct CreateStationView_Previews: PreviewProvider {
//    static var previews: some View {
//        CreateStationView()
//    }
//}
