//
//  BusinessView.swift
//  MotoChat
//
//  Created by Justin Bunde on 25.07.23.
//

import SwiftUI

struct BusinessView: View {
    @ObservedObject var tourDetailpageViewModel = TourDetailpageViewModel()
    @ObservedObject var stationViewModel = StationViewModel()
    @Environment(\.presentationMode) var presentationMode
    @State var shouldShowAddCafeDialog = false
    @State var shouldShowTourPlanDialog = false
    @Binding var selected_annotation: Annotation
    var body: some View {
        VStack {
            HStack(alignment: .top) {
                VStack (alignment: .leading) {
                    Text(selected_annotation.name)
                        .font(.title)
                    Text(selected_annotation.address)
                }
                Spacer()
                
                Button {
                    shouldShowTourPlanDialog.toggle()
                } label: {
                    Text("Tour planen")
                }
                if !selected_annotation.isRegistered {
                    Button {
                        shouldShowAddCafeDialog.toggle()
                    } label: {
                        Image(systemName: "person.crop.circle.fill.badge.plus")
                            .foregroundColor(.secondary)
                    }
                }
                Button {
                    presentationMode.wrappedValue.dismiss()
                } label: {
                    Image(systemName: "xmark.circle.fill")
                        .foregroundColor(.secondary)
                }
            }
            
            HStack{
                ForEach(tourDetailpageViewModel.actions) { action in
                    Button {
                        action.handler()
                    } label: {
                        VStack {
                            Image(systemName: action.image)
                            Text(action.title)
                        }
                        .frame(maxWidth: .infinity)
                    }
                    .buttonStyle(.bordered)
                }
            }
        }
        .padding()
        .confirmationDialog("Möchtest du das Cafe hinzufügen?", isPresented: $shouldShowAddCafeDialog, titleVisibility: .visible, actions: {
            Button("Cafe hinzufügen") {
                stationViewModel.addBusiness(annotation: selected_annotation)
                shouldShowAddCafeDialog.toggle()
                presentationMode.wrappedValue.dismiss()
            }
        })
        .onAppear{
            tourDetailpageViewModel.createActions(business: selected_annotation)
        }
        .sheet(isPresented: $shouldShowTourPlanDialog) {
            CreateTourView(searchQuery: selected_annotation.name)
        }
    }
}

//struct BusinessView_Previews: PreviewProvider {
//    static var previews: some View {
//        BusinessView()
//    }
//}
