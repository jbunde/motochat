//
//  TourDetailpageViewModel.swift
//  MotoChat
//
//  Created by Justin Bunde on 07.07.23.
//

import SwiftUI
import MapKit
import CoreLocation

struct TourDetailpage: View {
    @ObservedObject var tourDetailpageViewModel = TourDetailpageViewModel()
    @Binding var selectedTour: TourCardViewModel
    @State var selected_annotation = Annotation(name: "String", coordinate: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), phoneNumber: "String", website: URL(string: "https://www.example.com"), isRegistered: false, address: "String", isCafe: false)
    @State private var screenWidth: CGFloat = UIScreen.main.bounds.width
    let locationManager = CLLocationManager()
    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), latitudinalMeters: 4000, longitudinalMeters: 4000)

    var body: some View {
        NavigationView {
            ScrollView {
                TourCard(tourCardViewModel: selectedTour)
                VStack(alignment: .leading) {
                    Text("Startadresse:").fontWeight(.bold).padding(.bottom, 5).padding(.leading, 5).padding(.trailing, 5)
                    Text(selectedTour.tour.annotation_address[0]).padding(.bottom, 5).padding(.leading, 5).padding(.trailing, 5)
                    Text("Adressen der Zwischenziele: ").fontWeight(.bold).padding(.bottom, 5).padding(.leading, 5).padding(.trailing, 5)
                    ForEach(selectedTour.tour.annotation_address.dropFirst().dropLast(), id: \.self) { stop in
                        VStack(alignment: .leading) {
                            Text("- " + stop).padding(.bottom, 5).padding(.bottom, 5).padding(.leading, 5).padding(.trailing, 5)
                        }
                    }
                    Text("Zieladresse:").fontWeight(.bold).padding(.bottom, 5).padding(.leading, 5).padding(.trailing, 5)
                    Text(selectedTour.tour.annotation_address[selectedTour.tour.annotation_address.count-1]).padding(.bottom, 5).padding(.leading, 5).padding(.trailing, 5)
                }
                
                Map(coordinateRegion: $region,
                    showsUserLocation: true,
                    annotationItems: tourDetailpageViewModel.createAnnotations(selectedTour: selectedTour)) { annotation in
                    MapAnnotation(coordinate: annotation.coordinate) {
                        Image(systemName: "mappin.and.ellipse")
                            .font(.title)
                            .foregroundColor(.blue)
                            .onTapGesture {
                                selected_annotation = annotation
                                tourDetailpageViewModel.isMapViewShowing.toggle()
                            }
                        Text(annotation.name)
                    }
                }
                .overlay(
                    GeometryReader { geometry in
                        Path { path in
                            let startPoint = tourDetailpageViewModel.startPoint(in: geometry, selectedTour: selectedTour, region: region)
                            path.move(to: startPoint)
                            for annotation in tourDetailpageViewModel.createAnnotations(selectedTour: selectedTour) {
                                let point = CGPoint(x: geometry.size.width * (annotation.coordinate.longitude - region.center.longitude) / region.span.longitudeDelta + geometry.size.width / 2,
                                                    y: geometry.size.height * (region.center.latitude - annotation.coordinate.latitude) / region.span.latitudeDelta + geometry.size.height / 2)
                                path.addLine(to: point)
                            }
                        }
                        .stroke(Color.blue, lineWidth: 3)
                    }
                )
                .frame(width: screenWidth - 30, height: 400)
                .cornerRadius(10)
                
            }
            .onAppear() {
                region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: selectedTour.tour.annotation_coordinates_lat[0], longitude: selectedTour.tour.annotation_coordinates_long[0]), latitudinalMeters: 5000, longitudinalMeters: 5000)
                locationManager.requestAlwaysAuthorization()
                screenWidth = UIScreen.main.bounds.width
                
            }
        }
            .sheet(isPresented: $tourDetailpageViewModel.isMapViewShowing) {
                    BusinessView(selected_annotation: $selected_annotation)
                        .presentationDetents([.fraction(0.27), .large])
            }
    }
}
    


//struct TourDetailpageViewModel_Previews: PreviewProvider {
//    static var previews: some View {
//        TourDetailpageViewModel()
//    }
//}
