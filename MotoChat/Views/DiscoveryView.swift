//
//  DiscoveryView.swift
//  MotoChat
//
//  Created by Justin Bunde on 26.07.23.
//

import SwiftUI
import MapKit

struct DiscoveryView: View {
    @ObservedObject var discoveryViewModel = DiscoveryViewModel()
    @State var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), latitudinalMeters: 500, longitudinalMeters: 500)
    let locationManager = CLLocationManager()
    @State var selected_business = Annotation(name: "String", coordinate: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), phoneNumber: "String", website: URL(string: "https://www.example.com"), isRegistered: false, address: "String", isCafe: false)
    @State var isShowingBusinessView = false
    @State var counter = 0
    @State var isShowingCreateStation = false
    
    
    var body: some View {
        NavigationView {
            VStack {
                Map(coordinateRegion: $region,
                    showsUserLocation: true,
                    annotationItems: discoveryViewModel.businesses) { business in
                    MapAnnotation(coordinate: business.coordinate) {
                        Image(systemName: "cup.and.saucer.fill")
                            .font(.title)
                            .foregroundColor(business.isCafe ? (business.isRegistered ? .yellow : .pink) : .blue)
                            .onTapGesture {
                                selected_business = business
                                isShowingBusinessView.toggle()
                            }
                    }
                }
            }.navigationTitle("Entdecken")
                .toolbar {
                    ToolbarItem(placement: .navigationBarTrailing, content: {
                        Button {
                            isShowingCreateStation.toggle()
                        } label: {
                            Text("Business manuell registrieren")
                        }

                    })
                }

        }
        .onAppear {
                Task {
                    discoveryViewModel.businesses = []
                    await discoveryViewModel.searchPlaces(query: "Berlin, Germany")
                    region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: discoveryViewModel.places[0].placemark.coordinate.latitude , longitude: discoveryViewModel.places[0].placemark.coordinate.longitude), latitudinalMeters: 2500, longitudinalMeters: 2500)
                    await discoveryViewModel.searchBusinesses(region: region)
                }
            
        }.sheet(isPresented: $isShowingBusinessView) {
            BusinessView(selected_annotation: $selected_business)
                .presentationDetents([.fraction(0.27), .large])
        }
        .sheet(isPresented: $isShowingCreateStation) {
            RegisterStationView().onDisappear{
                Task {
                    discoveryViewModel.businesses = []
                    await discoveryViewModel.searchPlaces(query: "Berlin, Germany")
                    region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: discoveryViewModel.places[0].placemark.coordinate.latitude , longitude: discoveryViewModel.places[0].placemark.coordinate.longitude), latitudinalMeters: 2500, longitudinalMeters: 2500)
                    await discoveryViewModel.searchBusinesses(region: region)
                }
            }
        }
    }
}

