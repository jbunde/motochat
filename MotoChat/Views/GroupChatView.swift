//
//  GroupChatView.swift
//  MotoChat
//
//  Created by Justin Bunde on 26.07.23.
//

import SwiftUI

struct GroupChatView: View {
    @ObservedObject var groupChatViewModel = GroupChatViewModel()
    @State private var messageText = ""
    @State private var senderName = "Justin"
    @Binding var selectedTour: TourCardViewModel
    
    var body: some View {
        NavigationView {
            VStack {
                ScrollView {
                    LazyVStack(spacing: 0) {
                        ForEach(groupChatViewModel.messages) { message in
                            MessageRow(message: message, isCurrentUser: message.sender == senderName)
                                .padding(.vertical, 4)
                                .background(Color.clear)
                        }
                    }
                }
                
                HStack {
                    TextField("Type your message...", text: $messageText)
                    Button("Senden") {
                        Task {
                            if let tourId = selectedTour.tour.id {
                                await groupChatViewModel.addMessage(message: messageText, sender: senderName, id: tourId)
                            }
                            messageText = ""
                        }
                    }
                }
                .padding()
            }.navigationBarTitle(selectedTour.tour.title)
        }
        .onAppear {
            if let tourId = selectedTour.tour.id {
                groupChatViewModel.getGroupMessages(id: tourId)
            }
        }

    }
}

//struct GroupChatView_Previews: PreviewProvider {
//    static var previews: some View {
//        GroupChatView()
//    }
//}
