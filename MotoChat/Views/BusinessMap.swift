//
//  BusinessMap.swift
//  MotoChat
//
//  Created by Justin Bunde on 11.07.23.
//

import SwiftUI
import MapKit
import CoreLocation

struct BusinessMap: View {
    @Environment(\.presentationMode) private var presentationMode
    @ObservedObject var tourDetailpageViewModel = TourDetailpageViewModel()
    @ObservedObject var businessMapViewModel = BusinessMapViewModel()
    @State var isShowingCreateStation = false
    @Binding var stop_annotations: [MKMapItem]
    @Binding var start_annotations: MKMapItem
    @Binding var destination_annotations: MKMapItem
    @State var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), latitudinalMeters: 500, longitudinalMeters: 500)
    @State var selected_business = Annotation(name: "String", coordinate: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0), phoneNumber: "String", website: URL(string: "https://www.example.com"), isRegistered: false, address: "String", isCafe: false)
    @State var isShowingBusinessView = false

    var body: some View {
        HStack() {
            Button {
                presentationMode.wrappedValue.dismiss()
            } label: {
                Text("Schließen")
            }
            Spacer()
                Button {
                    isShowingCreateStation.toggle()
                } label: {
                    Text("Cafe registrieren")
                }
            }
            .padding(.all, 15)
            .onAppear {
                Task {
                    region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: start_annotations.placemark.coordinate.latitude, longitude: start_annotations.placemark.coordinate.longitude), latitudinalMeters: 2000, longitudinalMeters: 2000)
                    await businessMapViewModel.searchBusinesses(region: region, stop_annotations: stop_annotations, start_annotation: start_annotations, destination_annotation: destination_annotations)
                }
            }

        Map(coordinateRegion: $region,
            showsUserLocation: true,
            annotationItems: businessMapViewModel.businesses) { business in
            MapAnnotation(coordinate: business.coordinate) {
                Image(systemName: business.isCafe ? "cup.and.saucer.fill" : "mappin.and.ellipse")
                    .font(.title)
                    .foregroundColor(business.isCafe ? (business.isRegistered ? .yellow : .pink) : .blue)
                    .onTapGesture {
                        selected_business = business
                        isShowingBusinessView.toggle()
                    }
                Text(!business.isCafe ? business.name : "")
            }
        }
            .overlay(
                GeometryReader { geometry in
                    Path { path in
                        if businessMapViewModel.annotations.isEmpty {return}
                        let startPoint = businessMapViewModel.startPoint(in: geometry, region: region)
                        path.move(to: startPoint)
                        for annotation in businessMapViewModel.annotations {
                            let point = CGPoint(x: geometry.size.width * (annotation.coordinate.longitude - region.center.longitude) / region.span.longitudeDelta + geometry.size.width / 2,
                                                y: geometry.size.height * (region.center.latitude - annotation.coordinate.latitude) / region.span.latitudeDelta + geometry.size.height / 2)
                            path.addLine(to: point)
                        }
                    }
                    .stroke(Color.blue, lineWidth: 3)
                }
            )
            .id(businessMapViewModel.mapID) // Aktualisiere die ID, wenn sich die Daten geändert haben
        .edgesIgnoringSafeArea(.bottom)
        .onReceive(businessMapViewModel.$businesses) { _ in
            businessMapViewModel.mapID = UUID()
        }

        .sheet(isPresented: $isShowingBusinessView) {
            BusinessView(selected_annotation: $selected_business)
                .presentationDetents([.fraction(0.27), .large])
        }
        .sheet(isPresented: $isShowingCreateStation) {
            RegisterStationView().onDisappear{
                Task {
                    businessMapViewModel.businesses = []
                    businessMapViewModel.annotations = []
                    region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: start_annotations.placemark.coordinate.latitude, longitude: start_annotations.placemark.coordinate.longitude), latitudinalMeters: 2000, longitudinalMeters: 2000)
                    await businessMapViewModel.searchBusinesses(region: region, stop_annotations: stop_annotations, start_annotation: start_annotations, destination_annotation: destination_annotations)
                }
            }
        }
    }
    
}


