//
//  TourCard.swift
//  MotoChat
//
//  Created by Justin Bunde on 30.06.23.
//

import SwiftUI
import Foundation


struct TourCard: View {
    @ObservedObject var tourCardViewModel: TourCardViewModel
    var body: some View {
        VStack {
            HStack {
                Text(tourCardViewModel.tour.title)
                if(tourCardViewModel.tour.exclusive){
                    Image(systemName: "lock")
                }
            }
            Divider()
            HStack {
                HStack {
                    VStack(spacing: 5) {
                        HStack {
                            Text("Start: \(tourCardViewModel.tour.annotation_name[0])")
                                .font(.callout)
                                .lineLimit(1)
                            Spacer()
                            Text(tourCardViewModel.getDate(startingDate: true))
                                .font(.caption)
                                .lineLimit(1)
                        }
                        HStack {
                            Text("Ziel: \(tourCardViewModel.tour.annotation_name[tourCardViewModel.tour.annotation_name.count-1])")
                                .font(.callout)
                                .lineLimit(1)
                            Spacer()
                            Text(tourCardViewModel.getDate(startingDate: false))
                                .font(.caption)
                                .lineLimit(1)
                        }
                    }
                }
            }
            Divider()
            HStack() {
                Group() {
                    Spacer()
                    Text("\(tourCardViewModel.tour.count_persons) von \(tourCardViewModel.tour.max_persons)")
                        .font(.caption)
                        .lineLimit(1)
                    Image(systemName: "person.fill")
                }
                Spacer()
                Group() {
                    
                    Text("\(tourCardViewModel.tour.age_restriction)")
                        .font(.caption)
                        .lineLimit(1)
                    Image(systemName: "hourglass")
                    Spacer()
                }
            }
        }
        .padding()
        .background(Rectangle().fill(Color("ITEM")))
        .cornerRadius(10)
        .shadow(color: Color("ITEM"), radius: 1, x: 2, y: 2)
        .padding()
    }
}

//struct TourCard_Previews: PreviewProvider {
//    static var previews: some View {
//        TourCard(tourCardViewModel: TourCardViewModel)
//    }
//}
