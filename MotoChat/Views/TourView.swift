//
//  TourView.swift
//  MotoChat
//
//  Created by Justin Bunde on 24.06.23.
//

import SwiftUI


struct TourView: View {
    @ObservedObject var tourViewModel = TourViewModel()
    
    @State var shouldShowTourDPViewModel = false
    @State var shouldShowCreateTourView = false
    @State var shouldShowEditTourView = false
    @State var selectedTour = TourCardViewModel(tourParam:
                                                    Tour(title: "String",
                                                         exclusive: false,
                                                         start_date: Date(),
                                                         end_date: Date(),
                                                         max_persons: "5",
                                                         age_restriction: "5",
                                                         annotation_coordinates_lat: [1.2],
                                                         annotation_coordinates_long: [1.2],
                                                         annotation_name: ["String"],
                                                         annotation_address: ["String"],
                                                         annotation_phone_number: ["String"],
                                                         annotation_website: [URL(string: "https://www.example.com")]))
    @State private var isLoading = false
    @State var selectedID = ""
    var body: some View {
        NavigationView {
            ScrollView{
                if isLoading {ProgressView()}
                ForEach(tourViewModel.tourCardViewModelArray, id: \.self) { tour in
                    TourCard(tourCardViewModel : tour)
                        .onTapGesture {
                            selectedTour = tour
                            shouldShowTourDPViewModel.toggle()
                        }.contextMenu {
                            Button {
                                Task {
                                    selectedTour = tour
                                    selectedID = selectedTour.tour.id ?? ""
                                    shouldShowEditTourView.toggle()
                                }
                            } label: {
                                Label("Tour bearbeiten", systemImage: "pencil")
                            }
                            Button {
                                Task {
                                    await tourViewModel.removeTour(id: tour.tour.id!)
                                }
                            } label: {
                                Label("Tour entfernen", systemImage: "trash.fill")
                            }
                        }
                }

            }.navigationTitle("Touren Übersicht")
                .toolbar {
                    ToolbarItem(placement: .navigationBarTrailing, content: {
                            Button {
                                shouldShowCreateTourView.toggle()
                            } label: {
                                Image(systemName: "calendar.badge.plus")
                            }
                    })
                }
        }.onAppear{
            Task {
                isLoading = true
                await tourViewModel.getTours()
                isLoading = false
            }
        }.sheet(isPresented: $shouldShowTourDPViewModel, onDismiss: {
            }, content: {
                if selectedTour.tour.count_persons == 1 {
                    TourDetailpage(selectedTour: self.$selectedTour)
                } else {
                    GroupChatView(selectedTour: self.$selectedTour)
                }
        })
        .sheet(isPresented: $shouldShowCreateTourView) {
            CreateTourView()
                .onDisappear {
                    Task {
                        isLoading = true
                        await tourViewModel.getTours()
                        isLoading = false
                    }
                }
        }
        .sheet(isPresented: $shouldShowEditTourView) {
            EditTourView(id: $selectedID)
                .onDisappear {
                Task {
                    tourViewModel.tourCardViewModelArray = []
                    isLoading = true
                    await tourViewModel.getTours()
                    isLoading = false
                }
            }
        }
    }
}

//struct TourView_Previews: PreviewProvider {
//    static var previews: some View {
//        TourView()
//    }
//}
