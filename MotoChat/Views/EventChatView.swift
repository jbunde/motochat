//
//  EventChatView.swift
//  MotoChat
//
//  Created by Justin Bunde on 12.07.23.
//

import SwiftUI

struct EventChatView: View {
    @ObservedObject var eventChatViewModel = EventChatViewModel()
    @State private var messageText = ""
    @State private var senderName = "Business"
    @State private var showDetailpage = false
    @State private var currentMessage = EventMessage(text: "String", sender: "String", timestamp: Date())
    
    var body: some View {
        NavigationView {
            VStack {
                ScrollView {
                    LazyVStack(spacing: 0) {
                        ForEach(eventChatViewModel.messages) { message in
                            MessageRow(message: message, isCurrentUser: message.sender == senderName)
                                .padding(.vertical, 4)
                                .background(Color.clear)
                                .onTapGesture {
                                    currentMessage = message
                                    showDetailpage.toggle()
                                }
                        }
                    }
                }
                
                HStack {
                    TextField("Type your message...", text: $messageText)
                    Button("Senden") {
                        Task {
                            await eventChatViewModel.addMessage(message: messageText, sender: senderName)
                            messageText = ""
                        }
                    }
                }
                .padding()
            }.navigationBarTitle("Event-Chat")
        }
        .onAppear {
            eventChatViewModel.getEventMessages()
        }.sheet(isPresented: $showDetailpage) {
            EventChatDetailpage(selectedMessage: $currentMessage)
        }

    }
}

