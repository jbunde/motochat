//
//  CreateTourView.swift
//  MotoChat
//
//  Created by Justin Bunde on 09.07.23.
//

import SwiftUI
import MapKit

struct CreateTourView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var shouldShowBusinessesMap = false
    @State var showMapButton = false
    @State private var startDate = Date()
    @State private var endDate = Date()
    @State private var title = ""
    @State var searchQuery = ""
    @State private var searchQueryStops = ""
    @State private var searchQueryEnd = ""
    @State private var finalStart = ""
    @State private var finalDestination = ""
    @State private var finalStops = [String] ()
    @State private var isExclusive = false
    @State private var selectedFSK = "Keine Beschränkung"
    @State private var selectedMaxPersons = "5"
    @State private var annotation_start = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: 0, longitude: 0)))
    @State private var annotation_destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: 0, longitude: 0)))
    @State private var annotation_stops = [MKMapItem] ()
    @StateObject var createTourViewModel = CreateTourViewModel()
    let fsk = ["Keine Beschränkung", "ab 18 Jahren", "ab 24 Jahren"]
    let maxPersons = ["5", "10", "25", "50", "100", "250", "500"]
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack {
                    VStack {
                        VStack(alignment: .leading) {
                            Group {
                                Text("Titel der Tour").fontWeight(.bold)
                                TextField("Titel", text: $title)
                                    .textFieldStyle(RoundedBorderTextFieldStyle())
                                    .padding(.bottom, 15)
                            }
                            Group {
                                Text("Suche").fontWeight(.bold)
                                TextField("Von wo und wohin soll es gehen?", text: $searchQuery)
                                    .textFieldStyle(RoundedBorderTextFieldStyle())
                                    .onChange(of: searchQuery) { query in
                                        createTourViewModel.searchPlaces(query: query)
                                    }.padding(.bottom, 15)
                                HStack {
                                    Text("Startpunkt: ").fontWeight(.bold)
                                    Text(finalStart)
                                }.padding(.bottom, 15)
                                HStack {
                                    Text("Ziel: ").fontWeight(.bold).fontWeight(.bold)
                                    Text(finalDestination)
                                }.padding(.bottom, 15)
                            }.onAppear{
                                createTourViewModel.searchPlaces(query: searchQuery)
                            }
                            Group {
                                Text("Zwischenziele: ").padding(.bottom, 15).fontWeight(.bold)
                                    ForEach(finalStops, id: \.self) { stop in
                                        VStack(alignment: .leading) {
                                            Text("- " + stop).padding(.bottom, 2)
                                        }
                                    }
                            }
                            if showMapButton {
                                Button("Startpunkt auf der Karte anzeigen") {
                                    shouldShowBusinessesMap.toggle()
                                }
                            }
                            Divider()
                            LazyVStack(alignment: .center) {
                                ForEach(createTourViewModel.places, id: \.self) { place in
                                    VStack(alignment: .leading) {
                                        Text(place.name ?? "")
                                            .font(.headline)
                                        Text(place.placemark.title ?? "")
                                            .font(.subheadline)
                                        Group {
                                            Button("Bei Startpunkt hinzufügen") {
                                                searchQuery = place.name ?? "default value"
                                                annotation_start = place
                                                finalStart = searchQuery
                                                showMapButton = true
                                            }
                                            Button("Bei Ziel hinzufügen") {
                                                searchQuery = place.name ?? "default value"
                                                annotation_destination = place
                                                finalDestination = searchQuery
                                            }
                                            Button("Neues Zwischenziel") {
                                                searchQuery = place.name ?? "default value"
                                                annotation_stops.append(place)
                                                finalStops.append(searchQuery)
                                            }
                                            Divider()
                                        }.padding(.top, 2)
                                    }.padding()
                                }
                            }
                        }
                    }
                    VStack (alignment: .leading) {
                        HStack {
                            DatePicker(selection: $startDate, displayedComponents: [.date, .hourAndMinute]) {
                                Text("Startdatum").fontWeight(.bold)
                            }.padding(.bottom, 15)
                        }
                        HStack {
                            DatePicker(selection: $endDate, displayedComponents: [.date, .hourAndMinute]) {
                                Text("Enddatum").fontWeight(.bold)
                            }.padding(.bottom, 15)
                        }
                        
                        HStack {
                            Text("Altersbeschränkung").fontWeight(.bold)
                            Picker("Wähle eine Option", selection: $selectedFSK) {
                                ForEach(0..<fsk.count) { index in
                                    Text(fsk[index]).tag(fsk[index])
                                }
                            }
                        }.padding(.bottom, 15)
                        
                        HStack {
                            Text("Max. Teilnehmeranzahl").fontWeight(.bold)
                            Picker("Wähle eine Option", selection: $selectedMaxPersons) {
                                ForEach(0..<maxPersons.count) { index in
                                    Text(maxPersons[index]).tag(maxPersons[index])
                                }
                            }
                        }.padding(.bottom, 15)
                        
                        HStack {
                            Toggle("Private Tour", isOn: $isExclusive)
                                .fontWeight(.bold)
                                .padding(.bottom, 15)
                        }
                    }
                    Button {
                        Task {
                            await createTourViewModel.handleNewTour(title: self.title, exclusive: self.isExclusive, start_date: self.startDate, end_date: self.endDate, starting_point: self.annotation_start, destination: self.annotation_destination, maxPersons: self.selectedMaxPersons, ageRestriction: self.selectedFSK, stops: self.annotation_stops)
                        }
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        Text("Tour erstellen").fontWeight(.bold)
                    }.padding()
                }.padding()
            }.navigationTitle("Tour erstellen")
                .sheet(isPresented: $shouldShowBusinessesMap) {
                    BusinessMap(stop_annotations: self.$annotation_stops, start_annotations: self.$annotation_start, destination_annotations: self.$annotation_destination)
                }
        }
    }
}

//struct CreateTourView_Previews: PreviewProvider {
//    static var previews: some View {
//        CreateTourView()
//    }
//}
