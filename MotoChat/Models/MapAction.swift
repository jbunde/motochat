//
//  MapAction.swift
//  MotoChat
//
//  Created by Justin Bunde on 11.07.23.
//

import Foundation

struct MapAction: Identifiable {
    let id = UUID()
    let title: String
    let image: String
    let handler: () -> Void
}
