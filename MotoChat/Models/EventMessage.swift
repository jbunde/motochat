//
//  MEssage.swift
//  MotoChat
//
//  Created by Justin Bunde on 26.07.23.
//

import Foundation

struct EventMessage: Codable, Identifiable {
    var id = UUID()
    var text: String
    var sender: String
    var timestamp: Date
}
