//
//  User.swift
//  MotoChat
//
//  Created by Justin Bunde on 24.06.23.
//

import Foundation

struct User {
    var surname: String
    var lastname: String
    var sex: String
    var profile_picture: Any
    var about_user: String
    var role: String
    var birthday: Date
    var residence: String
}
