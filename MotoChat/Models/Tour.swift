//
//  Tour.swift
//  MotoChat
//
//  Created by Justin Bunde on 24.06.23.
//

import Foundation
import FirebaseFirestoreSwift


struct Tour: Identifiable, Codable, Hashable{
    @DocumentID var id: String?
    var title: String
    var exclusive: Bool
    var start_date: Date
    var end_date: Date
    var max_persons: String
    var count_persons = 1
    var age_restriction: String
    var annotation_coordinates_lat: [Double]
    var annotation_coordinates_long: [Double]
    var annotation_name: [String]
    var annotation_address: [String]
    var annotation_phone_number: [String?]
    var annotation_website: [URL?]
}
