//
//  Annotation.swift
//  MotoChat
//
//  Created by Justin Bunde on 08.07.23.
//

import Foundation
import MapKit

struct Annotation: Identifiable {
    let id = UUID()
    let name: String
    let coordinate: CLLocationCoordinate2D
    let phoneNumber: String?
    let website: URL?
    let isRegistered: Bool
    let address: String
    let isCafe: Bool
}
