//
//  MotoChatApp.swift
//  MotoChat
//
//  Created by Justin Bunde on 18.06.23.
//

import SwiftUI
import FirebaseCore


@main
struct MotoChatApp: App {
    init() {
        FirebaseApp.configure()
    }
    var body: some Scene {
        WindowGroup {
            ZStack {
                TabView {
                    TourView()
                        .tabItem {
                            Image(systemName: "location")
                            Text("Touren")
                        }
                    EventChatView()
                        .tabItem {
                            Image(systemName: "party.popper")
                            Text("Event-Chat")
                        }
                    DiscoveryView()
                        .tabItem {
                            Image(systemName: "map.fill")
                            Text("Entdecken")
                        }
                }
            }.preferredColorScheme(.dark)
            .edgesIgnoringSafeArea(.all)
        }
    }
}
