//
//  DiscoveryViewModel.swift
//  MotoChat
//
//  Created by Justin Bunde on 26.07.23.
//

import Foundation
import MapKit
import SwiftUI

class DiscoveryViewModel: ObservableObject {
    @Published var businesses = [Annotation] ()
    @ObservedObject var stationViewModel = StationViewModel()
    @Published var mapID = UUID()
    @State var isShowingBusinessView = false
    @Published var places: [MKMapItem] = []

    func searchBusinesses(region : MKCoordinateRegion) async {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = "cafe"
        request.region = region
        request.resultTypes = .pointOfInterest // Ergebnistyp auf "pointOfInterest" setzen
        
        do {
            let response = try await MKLocalSearch(request: request).start()

            var _ = response.mapItems.map({ item in
                    self.businesses.append(Annotation(name: item.name ?? "", coordinate: item.placemark.coordinate, phoneNumber: item.phoneNumber, website: item.url, isRegistered: false, address: item.placemark.title ?? "default title", isCafe: true))
                })
            Task {
                let stations = try await stationViewModel.getBusinesses()
                for station in stations {
                    self.businesses.append(station)
                }
            }

        } catch {
            // Fehlerbehandlung
            print("Fehler bei der lokalen Suche: \(error)")
        }
    }
    
    func searchPlaces(query: String) async {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = query

        do {
            let response = try await MKLocalSearch(request: request).start()
            let mapItems = response.mapItems
            
            self.places = mapItems
        } catch {
            print("Fehler bei der Suche: \(error.localizedDescription)")
        }
    }
}
