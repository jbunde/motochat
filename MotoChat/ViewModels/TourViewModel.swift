//
//  TourViewModel.swift
//  MotoChat
//
//  Created by Justin Bunde on 24.06.23.
//

import Foundation
import SwiftUI
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift
import CoreLocation

class TourViewModel: ObservableObject {
    @Published var tourArray: [Tour] = []
    @Published var tourCardViewModelArray: [TourCardViewModel] = []
    let db = Firestore.firestore()
    
    func addTour(tour: Tour) async {
        do {
            try db.collection("tours").addDocument(from: tour)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func getTours() async {
        do {
            let querySnapshot = try await db.collection("tours")
                .whereField("end_date", isGreaterThanOrEqualTo: Date())
                .order(by: "end_date", descending: false)
                .getDocuments()
            let documents = querySnapshot.documents
            self.tourArray = documents.compactMap { document -> Tour? in
                do {
                    return try document.data(as: Tour.self)
                } catch {
                    print("TourViewModel (35): \(error)")
                    return nil
                }
            }
            loadTours()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func loadTours() {
        let tours = tourArray.map { tour -> TourCardViewModel in
            return TourCardViewModel(tourParam: tour)
        }
        self.tourCardViewModelArray = tours
    }
    
    func removeTour(id: String) async {
        do {
            try await db.collection("tours").document(id).delete()
            tourCardViewModelArray = []
            await getTours()
        } catch {
            print("TourViewModel (58): \(error)")
        }
    }

    
    func updateTour(id: String, tourObject: Tour) async {
        do {
            try db.collection("tours").document(id).setData(from: tourObject)
            await getTours()
        } catch {
            print("TourViewModel (69): \(error)")
        }
    }
}

