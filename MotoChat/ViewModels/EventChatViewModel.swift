//
//  EventChatViewModel.swift
//  MotoChat
//
//  Created by Justin Bunde on 26.07.23.
//

import Foundation
import SwiftUI
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift


class EventChatViewModel: ObservableObject {
    @Published var messages = [EventMessage]()
    private let db = Firestore.firestore()
    
    func addMessage(message: String, sender: String) async {
        do {
            try db.collection("eventmessages").addDocument(from: EventMessage(text: message, sender: sender, timestamp: Date()))
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func getEventMessages () {
        db.collection("eventmessages").order(by: "timestamp")
            .addSnapshotListener({ (querySnapshot, error) in
                if error != nil {
                    print(error?.localizedDescription ?? "Error while fetching messages")
                    return
                }
                guard let documents = querySnapshot?.documents else {
                    print("no Documents")
                    return
                }
                self.messages = documents.compactMap { document -> EventMessage? in
                    do {
                        return try document.data(as: EventMessage.self)
                    } catch {
                        print("Error decoding document into message: \(error)")
                        return nil
                    }
                }
            })
    }
}


