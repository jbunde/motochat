//
//  RegisterStationViewModel.swift
//  MotoChat
//
//  Created by Justin Bunde on 26.07.23.
//

import Foundation
import MapKit
import SwiftUI

class RegisterStationViewModel: ObservableObject {
    @Published var places: [MKMapItem] = []
    
    func searchPlaces(query: String) {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = query
 
        let search = MKLocalSearch(request: request)
        search.start { response, error in
            if let error = error {
                print("Fehler bei der Suche: \(error.localizedDescription)")
                return
            }
 
            guard let mapItems = response?.mapItems else {
                return
            }
 
            DispatchQueue.main.async {
                self.places = mapItems
            }
        }
    }
}
