//
//  TourDetailpageViewModel.swift
//  MotoChat
//
//  Created by Justin Bunde on 08.07.23.
//
 
import Foundation
import CoreLocation
import SwiftUI
import MapKit

class TourDetailpageViewModel: ObservableObject {
    @Published var isMapViewShowing = false
    @Published var actions = [MapAction] ()
    @ObservedObject var stationViewModel = StationViewModel()
    
    func createAnnotations(selectedTour: TourCardViewModel) -> [Annotation] {
        var annotationArray = [Annotation] ()
        var currentStop: CLLocationCoordinate2D

        for i in 0..<selectedTour.tour.annotation_coordinates_lat.count {
            currentStop = CLLocationCoordinate2D(latitude: selectedTour.tour.annotation_coordinates_lat[i], longitude: selectedTour.tour.annotation_coordinates_long[i])
            annotationArray.append(Annotation(name: selectedTour.tour.annotation_name[i],
                                            coordinate: currentStop,
                                            phoneNumber: selectedTour.tour.annotation_phone_number[i],
                                            website: selectedTour.tour.annotation_website[i],
                                            isRegistered: false,
                                              address: selectedTour.tour.annotation_address[i],
                                              isCafe: false))
        }
        return annotationArray
    }
    
    func startPoint(in geometry: GeometryProxy, selectedTour: TourCardViewModel, region: MKCoordinateRegion) -> CGPoint {
        let x = geometry.size.width * (selectedTour.tour.annotation_coordinates_long[0] - region.center.longitude) / region.span.longitudeDelta + geometry.size.width / 2
        let y = geometry.size.height * (region.center.latitude - selectedTour.tour.annotation_coordinates_lat[0]) / region.span.latitudeDelta + geometry.size.height / 2
        return CGPoint(x: x, y: y)
    }
    
    
    
    func openMap(coordinate: CLLocationCoordinate2D) {
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate))
        mapItem.openInMaps()
    }
    
    
    func createActions(business: Annotation) {
        var actions = [MapAction]()

        actions.append(MapAction(title: "Apple Maps", image: "map.fill") { [weak self] in
            guard let self = self else { return }
            self.openMap(coordinate: business.coordinate)
        })

        if let phoneNumber = business.phoneNumber {
            let callAction = MapAction(title: "Anrufen", image: "phone.fill") { [weak self] in
                guard let self = self else { return }
                guard let url = URL(string: self.convertPhoneNumberFormat(phoneNumber: phoneNumber)) else { return }
                UIApplication.shared.open(url)
            }
            actions.append(callAction)
        }

        if let website = business.website {
            let websiteAction = MapAction(title: "Webseite", image: "safari.fill") { [weak self] in
                guard let self = self else { return }
                UIApplication.shared.open(website)
            }
            actions.append(websiteAction)
        }

        self.actions = actions
    }
    func convertPhoneNumberFormat(phoneNumber: String) -> String {
        let strippedPhoneNumber = phoneNumber
            .trimmingCharacters(in: .whitespacesAndNewlines)
            .components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
        return "tel:\(strippedPhoneNumber)"
    }
}
