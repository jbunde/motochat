//
//  StationViewModel.swift
//  MotoChat
//
//  Created by Justin Bunde on 26.07.23.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift
import CoreLocation

class StationViewModel:ObservableObject {
    @Published var stations = [Annotation] ()
    let db = Firestore.firestore()
    func addBusiness(annotation: Annotation) {
        let refDocument = db.collection("businesses").document()
        let id = refDocument.documentID
        let data: [String: Any] = [
            "id" : id,
            "name" : annotation.name,
            "coordinateLat" : annotation.coordinate.latitude,
            "coordinateLong" : annotation.coordinate.longitude,
            "phoneNumber": annotation.phoneNumber,
            "website": annotation.website?.absoluteString,
            "isRegistered": true,
            "address": annotation.address,
            "isCafe": true
        ]
        refDocument.setData(data) { Error in
            if let error = Error {
                print("Error adding new Station to Firebase \(error)")
            }
        }
    }
    
    func getBusinesses() async throws -> [Annotation] {
        let querySnapshot = try await db.collection("businesses").getDocuments()
        var businesses = [Annotation]()
        for document in querySnapshot.documents {
            var defaultUrl = URL(string: "https://www.defaultwebsite.com")
            let data = document.data()
            let websiteString = data["website"] as? String
            let finalURL = URL(string: websiteString ?? "") ?? defaultUrl
            businesses.append(Annotation(name: data["name"] as? String ?? "default name",
                                         coordinate: CLLocationCoordinate2D(latitude: data["coordinateLat"] as? Double ?? 0.0,
                                                                            longitude: data["coordinateLong"] as? Double ?? 0.0),
                                         phoneNumber: data["phoneNumber"] as? String,
                                         website: finalURL,
                                         isRegistered: data["isRegistered"] as? Bool ?? false,
                                         address: data["address"] as? String ?? "default address",
                                         isCafe: data["isCafe"] as? Bool ?? true))
        }
        return businesses
    }
}
