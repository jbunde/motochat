//
//  BusinessMapViewModel.swift
//  MotoChat
//
//  Created by Justin Bunde on 25.07.23.
//

import Foundation
import MapKit
import SwiftUI


class BusinessMapViewModel: ObservableObject {
    @Published var businesses = [Annotation] ()
    @Published var annotations = [Annotation] ()
    @ObservedObject var stationViewModel = StationViewModel()
    @Published var mapID = UUID()
    
    func searchBusinesses(region : MKCoordinateRegion, stop_annotations: [MKMapItem], start_annotation: MKMapItem, destination_annotation: MKMapItem) async {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = "cafe"
        request.region = region
        request.resultTypes = .pointOfInterest // Ergebnistyp auf "pointOfInterest" setzen

        do {
            let response = try await MKLocalSearch(request: request).start()
            annotations.append(Annotation(name: start_annotation.name ?? "default value", coordinate: CLLocationCoordinate2D(latitude: start_annotation.placemark.coordinate.latitude, longitude: start_annotation.placemark.coordinate.longitude), phoneNumber: start_annotation.phoneNumber, website: start_annotation.url, isRegistered: true, address: start_annotation.placemark.title ?? "default value", isCafe: false))
            
            annotations.append(Annotation(name: destination_annotation.name ?? "default value", coordinate: CLLocationCoordinate2D(latitude: destination_annotation.placemark.coordinate.latitude, longitude: destination_annotation.placemark.coordinate.longitude), phoneNumber: destination_annotation.phoneNumber, website: destination_annotation.url, isRegistered: true, address: destination_annotation.placemark.title ?? "default value", isCafe: false))
            
            for annotation in stop_annotations {
                self.annotations.append(Annotation(name: annotation.name ?? "default value", coordinate: CLLocationCoordinate2D(latitude: annotation.placemark.coordinate.latitude, longitude: annotation.placemark.coordinate.longitude), phoneNumber: annotation.phoneNumber, website: annotation.url, isRegistered: false, address: annotation.placemark.title ?? "default value", isCafe: false))
            }
            
            for annotation in annotations {
                self.businesses.append(annotation)
            }
            
                var _ = response.mapItems.map({ item in
                    self.businesses.append(Annotation(name: item.name ?? "", coordinate: item.placemark.coordinate, phoneNumber: item.phoneNumber, website: item.url, isRegistered: false, address: item.placemark.title ?? "default title", isCafe: true))
                })
            
            Task {
                let stations = try await stationViewModel.getBusinesses()
                for station in stations {
                    self.businesses.append(station)
                }
            }

        } catch {
            // Fehlerbehandlung
            print("Fehler bei der lokalen Suche: \(error)")
        }
    }

    
    func startPoint(in geometry: GeometryProxy, region: MKCoordinateRegion) -> CGPoint {
        let x = geometry.size.width * (annotations[0].coordinate.longitude - region.center.longitude) / region.span.longitudeDelta + geometry.size.width / 2
        let y = geometry.size.height * (region.center.latitude - annotations[0].coordinate.latitude) / region.span.latitudeDelta + geometry.size.height / 2
        return CGPoint(x: x, y: y)
    }

}


