//
//  TourCardViewModel.swift
//  MotoChat
//
//  Created by Justin Bunde on 02.07.23.
//

import Foundation
import Combine
import CoreLocation

class TourCardViewModel: ObservableObject, Hashable {
    var tour: Tour
    
    static func == (lhs: TourCardViewModel, rhs: TourCardViewModel) -> Bool {
        lhs.tour.id == rhs.tour.id
    }
        
    func hash(into hasher: inout Hasher) {
        hasher.combine(tour.id)
    }

    init(tourParam: Tour) {
        self.tour = tourParam
    }
    
    func getDate(startingDate: Bool) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "de")
        dateFormatter.dateFormat = "E dd.MM. HH:HH"
        if(startingDate) {
            return dateFormatter.string(from: self.tour.start_date)
        } else {
            return dateFormatter.string(from: self.tour.end_date)
        }
    }
}

