//
//  EditTourViewModel.swift
//  MotoChat
//
//  Created by Justin Bunde on 27.07.23.
//

import Foundation
import MapKit
import SwiftUI


class EditTourViewModel: ObservableObject {
    @ObservedObject var tourViewModel = TourViewModel()
    @Published var places: [MKMapItem] = []
    
    func searchPlaces(query: String) {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = query
 
        let search = MKLocalSearch(request: request)
        search.start { response, error in
            if let error = error {
                print("Fehler bei der Suche: \(error.localizedDescription)")
                return
            }
 
            guard let mapItems = response?.mapItems else {
                return
            }
 
            DispatchQueue.main.async {
                self.places = mapItems
            }
        }
    }
    
    func handleEditTour(id: String, title: String, exclusive: Bool, start_date: Date, end_date: Date, starting_point: MKMapItem, destination: MKMapItem, maxPersons: String, ageRestriction: String, stops: [MKMapItem] ) async {
        var annotation_coordinates_lat = [Double] ()
        var annotation_coordinates_long = [Double] ()
        var annotation_name = [String] ()
        var annotation_address = [String] ()
        var annotation_phone_number = [String?] ()
        var annotation_website = [URL?] ()
        
        annotation_coordinates_lat.append(starting_point.placemark.coordinate.latitude)
        annotation_coordinates_long.append(starting_point.placemark.coordinate.longitude)
        annotation_name.append(starting_point.name ?? "no name")
        annotation_address.append(starting_point.placemark.title ?? "no address")
        annotation_phone_number.append(starting_point.phoneNumber)
        annotation_website.append(starting_point.url)
        
        for stop in stops {
            annotation_coordinates_lat.append(stop.placemark.coordinate.latitude)
            annotation_coordinates_long.append(stop.placemark.coordinate.longitude)
            annotation_name.append(stop.name ?? "no name")
            annotation_address.append(stop.placemark.title ?? "no address")
            annotation_phone_number.append(stop.phoneNumber)
            annotation_website.append(stop.url)
        }
        annotation_coordinates_lat.append(destination.placemark.coordinate.latitude)
        annotation_coordinates_long.append(destination.placemark.coordinate.longitude)
        annotation_name.append(destination.name ?? "no name")
        annotation_address.append(destination.placemark.title ?? "no address")
        annotation_phone_number.append(destination.phoneNumber)
        annotation_website.append(destination.url)
        
        let newTour = Tour(title: title, exclusive: exclusive, start_date: start_date, end_date: end_date, max_persons: maxPersons, age_restriction: ageRestriction, annotation_coordinates_lat: annotation_coordinates_lat, annotation_coordinates_long: annotation_coordinates_long, annotation_name: annotation_name, annotation_address: annotation_address, annotation_phone_number: annotation_phone_number, annotation_website: annotation_website)
                           
        await tourViewModel.updateTour(id: id, tourObject: newTour)
    }
}
